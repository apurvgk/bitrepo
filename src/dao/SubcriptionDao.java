package dao;

import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.stereotype.Repository;

import model.EventResponse;
import model.SubscriptionEvent;

@Repository
public interface SubcriptionDao {

    EventResponse saveSubscriptionEventDetais(SubscriptionEvent eventDetails);

    SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier);

    EventResponse updateSubscriptionEventDetais(SubscriptionEvent eventDetails, String accIdentifier);

    EventResponse cancelSubscriptionEventDetais(String accIdentifier);

    ValueWrapper fetchValueWrapperForEvent(String cacheName, String accountIdentifier);
}
