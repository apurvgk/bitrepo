package common;

public enum ErrorCodes {

    ACCOUNT_NOT_FOUND("Account not found for given accountId."),
    USER_ALREADY_EXISTS("The user with given accountId already exists.");

    private String message;

    private ErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}