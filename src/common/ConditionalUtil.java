package common;

import java.util.List;

public class ConditionalUtil {

	public static final boolean isNullOrEmpty(String str) {
		if (null != str && !str.isEmpty()) {
            return false;
		}
        return true;
	}
}