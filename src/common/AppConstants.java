package common;

public class AppConstants {
    
    public static final String SUBSCRIPTION_EVENT_CACHE = "subscriptionEventCache";
    
    public static final String USER_CACHE = "usersCache";
    
    public static final String SUBS_ORDER_SUCCESS = "Subscription order added";
    
    public static final String SUBS_CHANGE_SUCCESS = "Subscription order Changed";
    
    public static final String SUBS_CANCELED_SUCCESS = "Subscription order cancelled";

    public static final String ACC_STATUS_ACTIVE = "ACTIVE";
    
    public static final String PREFIX_FOR_ACC_ID = "SE";



}
