package service;

import org.springframework.stereotype.Service;

import model.EventResponse;
import model.SubscriptionEvent;

@Service
public interface SubscriptionService {
    
    public EventResponse saveSubscriptionEventDetais(String url);

    public EventResponse updateSubscriptionEventDetais(String url);

    public EventResponse cancelSubscriptionEventDetais(String url);

    public SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier);
}
