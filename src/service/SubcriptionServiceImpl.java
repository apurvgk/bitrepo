package service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.Resource;

import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import common.AppConstants;
import common.ErrorCodes;
import dao.SubcriptionDao;
import model.EventResponse;
import model.SubscriptionEvent;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpRequest;
import oauth.signpost.signature.QueryStringSigningStrategy;

@Component
public class SubcriptionServiceImpl implements SubscriptionService {
    
    @Resource
    private SubcriptionDao dao;
    
    @Override
    public EventResponse saveSubscriptionEventDetais(String url) {
        SubscriptionEvent subscriptionEvent = fetchSubscriptionEventDetails(url);
        String accountIdentifier = extractAccountIdentifier(subscriptionEvent);
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accountIdentifier);
        if(null != valueWrapper){
            return createAndReturnErrorResponse(accountIdentifier ,ErrorCodes.USER_ALREADY_EXISTS);
        }
        return getDao().saveSubscriptionEventDetais(subscriptionEvent);
    }

    @Override
    public EventResponse updateSubscriptionEventDetais(String url) {
        SubscriptionEvent subscriptionEvent = fetchSubscriptionEventDetails(url);
        String accIdentifier = extractAccountIdentifier(subscriptionEvent);
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accIdentifier);
        if(null == valueWrapper){
            return createAndReturnErrorResponse(accIdentifier ,ErrorCodes.ACCOUNT_NOT_FOUND);
        }
        return getDao().updateSubscriptionEventDetais(subscriptionEvent,accIdentifier);
    }

    @Override
    public EventResponse cancelSubscriptionEventDetais(String url) {
        SubscriptionEvent subscriptionEvent = fetchSubscriptionEventDetails(url);
        String accIdentifier = extractAccountIdentifier(subscriptionEvent);
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accIdentifier);
        if(null == valueWrapper){
            return createAndReturnErrorResponse(accIdentifier ,ErrorCodes.ACCOUNT_NOT_FOUND);
        }
        return getDao().cancelSubscriptionEventDetais(accIdentifier);
    }
    
    @Override
    public SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier) {
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accountIdentifier);
        if (null == valueWrapper) {
            return new SubscriptionEvent();
        }
        return getDao().fetchEventInfoFromAccId(accountIdentifier);
    }
    
    public SubcriptionDao getDao() {
        return dao;
    }

    public void setDao(SubcriptionDao dao) {
        this.dao = dao;
    }

    private String extractAccountIdentifier(SubscriptionEvent eventDetails) {
        String accIdentifier = null != eventDetails.getAccount() ? eventDetails.getAccount().getAccountIdentifier() : null;
        return accIdentifier;
    }
    
    private EventResponse createAndReturnErrorResponse(String accIdentifier ,ErrorCodes errorCode){
        EventResponse response = new EventResponse();
        response.setAccountIdentifier(accIdentifier);
        response.setErrorCode(errorCode.toString());
        response.setMessage(errorCode.getMessage());
        response.setSuccess(false);
        return response;
    }
    
    /**
     * Kept protected for writing Junits
     * @param url
     * @return SubscriptionEvent Object from the JSON response returned by AppDirect service
     */
    protected SubscriptionEvent fetchSubscriptionEventDetails(String url) {
        RestTemplate restTemplate = new RestTemplate();
        OAuthConsumer consumer = new DefaultOAuthConsumer("Dummy", "secret");
        consumer.setSigningStrategy( new QueryStringSigningStrategy());
        SubscriptionEvent subscriptionEvent = new SubscriptionEvent() ;
        try {
        URL appDIrectUrl = new URL(url);
        HttpURLConnection request = (HttpURLConnection) appDIrectUrl.openConnection();
            HttpRequest signedHttpRequest = consumer.sign(request);
            ResponseEntity<SubscriptionEvent> response = restTemplate.exchange(signedHttpRequest.getRequestUrl(), HttpMethod.GET, null, SubscriptionEvent.class);
            subscriptionEvent = response.getBody();
        }
        catch (RestClientException | OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException e) {
            e.printStackTrace();
        }
        return subscriptionEvent;
    }
}