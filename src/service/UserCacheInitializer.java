package service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.stereotype.Service;

import common.AppConstants;
import model.User;

/**
 * This is a Temporary class just to initialize user cache which wont be required once a proper datastore setup is done.
 * 
 * @author apurv
 */
@Service
public class UserCacheInitializer {

    private static final String USER_CACHE_KEY = "users";

    @Resource(name = "cacheManager")
    private EhCacheCacheManager cacheManager;

    public EhCacheCacheManager getCacheManager() {
        return cacheManager;
    }

    @PostConstruct
    public void updateUserCache() {
        Cache userCache = getCacheManager().getCache(AppConstants.USER_CACHE);
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            User  user = new User();
            user.setEmail(i+"@test.com");
            user.setFirstName("FirstName"+i);
            user.setLanguage("language"+i);
            user.setLastName("LastName"+i);
            user.setLocale("locale"+i);
            user.setOpenId("openId"+1);
            user.setUuid("uuid"+i);
            users.add(user);
        }

        userCache.put(USER_CACHE_KEY, users);
    }

}
