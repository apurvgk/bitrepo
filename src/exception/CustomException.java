package exception;

public class CustomException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String message;

    private String errorCode;

    public CustomException(String errorCode,String message) {
        this.message = message;
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
    
    public String getErrorCode(){
        return errorCode;
    }
}
