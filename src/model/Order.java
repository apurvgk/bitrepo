package model;

import org.springframework.stereotype.Component;

@Component
public class Order {

    private String editionCode;

    private String addonOfferingCode;

    private String pricingDuration;

    public String getEditionCode() {
        return editionCode;
    }

    public void setEditionCode(String editionCode) {
        this.editionCode = editionCode;
    }

    public String getAddonOfferingCode() {
        return addonOfferingCode;
    }

    public void setAddonOfferingCode(String addonOfferingCode) {
        this.addonOfferingCode = addonOfferingCode;
    }

    public String getPricingDuration() {
        return pricingDuration;
    }

    public void setPricingDuration(String pricingDuration) {
        this.pricingDuration = pricingDuration;
    }

}
