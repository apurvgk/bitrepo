package model;

import org.springframework.stereotype.Component;

@Component
public enum SubscriptionType {
    SUBSCRIPTION_ORDER,
    SUBSCRIPTION_CHANGE,
    SUBSCRIPTION_CANCEL,
    SUBSCRIPTION_NOTICE,
    USER_ASSIGNMENT,
    USER_UNASSIGNMENT
}
