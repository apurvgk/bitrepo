package model;

import org.springframework.stereotype.Component;

@Component
public enum Flag {
    STATELESS,
    DEVELOPMENT
}
