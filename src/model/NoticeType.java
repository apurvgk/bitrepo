package model;

public enum NoticeType {

    REACTIVATED,
    DEACTIVATED,
    CLOSED

}
