package model;

import java.io.Serializable;
import java.util.Objects;

import org.springframework.stereotype.Component;

@Component
public class SubscriptionEvent implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private SubscriptionType type;

    private MarketPlace marketplace;

    private Creator creator;

    private Payload payload;
    
    private Account account;
    
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public SubscriptionType getType() {
        return type;
    }

    public void setType(SubscriptionType type) {
        this.type = type;
    }

    public MarketPlace getMarketplace() {
        return marketplace;
    }

    public void setMarketplace(MarketPlace marketplace) {
        this.marketplace = marketplace;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        return Objects.equals(this, (SubscriptionEvent) o);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this);
    }
}
