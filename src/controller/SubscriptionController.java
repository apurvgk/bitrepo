package controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import model.EventResponse;
import model.SubscriptionEvent;
import service.SubscriptionService;

@Controller
@RequestMapping("/subscription")
public class SubscriptionController {
    
    @Resource(name ="subcriptionServiceImpl")
    private SubscriptionService service;

    @RequestMapping(method = RequestMethod.GET, value = "/create", produces = "application/json")
    public @ResponseBody EventResponse createSubscription(@RequestParam String url) {
        EventResponse evResponse = getService().saveSubscriptionEventDetais(url);
        return evResponse;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/change", produces = "application/json")
    public @ResponseBody EventResponse changeSubscription(@RequestParam String url) {
        EventResponse evResponse = getService().updateSubscriptionEventDetais(url);
        return evResponse;
    } 
    
    @RequestMapping(method = RequestMethod.GET, value = "/cancel", produces = "application/json")
    public @ResponseBody EventResponse cancelSubscription(@RequestParam String url) {
        EventResponse evResponse = getService().cancelSubscriptionEventDetais(url);
        return evResponse;
    } 
    
    @RequestMapping(method = RequestMethod.GET, value = "/eventInfo", produces = "application/json")
    public @ResponseBody SubscriptionEvent fetchEventInfo(@RequestParam String accountIdentifier) {
        SubscriptionEvent eventDetails = getService().fetchEventInfoFromAccId(accountIdentifier);
        return eventDetails;
    }  
    
    @RequestMapping(method = RequestMethod.GET, value = "/status", produces = "application/json")
    public @ResponseBody EventResponse fetchStatus(@RequestParam String url, @RequestParam String token) {
        //TODO 
        return new EventResponse();
    }  
    
    @RequestMapping(method = RequestMethod.GET, value = "/assign", produces = "application/json")
    public @ResponseBody EventResponse assignUser(@RequestParam String url, @RequestParam String token) {
        //TODO 
        return new EventResponse();
    }  
    
    @RequestMapping(method = RequestMethod.GET, value = "/unassign", produces = "application/json")
    public @ResponseBody EventResponse unAssignUser(@RequestParam String url, @RequestParam String token) {
        //TODO 
        return new EventResponse();
    }  
    
    public SubscriptionService getService() {
        return service;
    }

    public void setService(SubscriptionService service) {
        this.service = service;
    }
}